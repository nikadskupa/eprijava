var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var run = require('gulp-run');
var runSequence = require('run-sequence');

gulp.task('sass', function() {
  return gulp.src('assets/sass/**/app.scss')
      .pipe(sass({
          outputStyle: 'compressed',
          includePaths: [
              	'node_modules/sass-mq',
    	          'node_modules/sass-bem',
                'node_modules/bulma/sass'
                ]
      }))
      .pipe(gulp.dest('dist/css'));
});

gulp.task('css', function() {
  gulp.src('dist/css/app.css')
    .pipe(cleanCSS())
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('live',['sass'], function() {
	gulp.watch('assets/sass/**/*.scss',function() {
        runSequence('sass');
    });
});